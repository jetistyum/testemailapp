<?php


namespace app\models\forms;


use app\models\File;
use app\models\Form;
use yii\base\Model;
use yii\web\UploadedFile;

class FileFormModel extends Model
{
    public  $title;
    public  $file;
    public  $filename;
    protected $form;

    public function rules()
    {
        return [
            ['title',       'string', 'length'=>[3,60]],
            ['file',        'each', 'rule'=>['file', 'skipOnEmpty' => false, 'maxFiles' => 1]],
            ['filename',    'each', 'rule'=>['string', 'length'=>[3,60]]],
        ];
    }

    public function save()
    {

        if ($this->validate()) {
            $form = new Form();
            $form->title = $this->title;
            $form->save();
            $this->form = $form;

            foreach ($this->file as $i=>$file) {
                /**
                 * @var UploadedFile $file
                 */
                $path = \Yii::getAlias(\Yii::$app->params['uploadPath']) .'/'. md5($file->baseName.microtime()). '.' . $file->extension;
                $file->saveAs($path);
                $fileModel = new File();
                $fileModel->filepath = $path;
                $fileModel->filename = $this->filename[$i];
                $fileModel->form_id = $form->id;
                $fileModel->save();
            }
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'title'=>'Form name',
        ];
    }



}