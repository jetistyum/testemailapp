<?php
namespace app\models\forms;

use app\models\Email;

class EmailForm extends \yii\base\Model
{
    public $email;
    protected $code;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
        ];
    }


    /**
     * handles form saving
     * @return bool
     */
    public function save(){
        $reg = \Yii::$app->reg;
        return  $reg->createEmail($this);
    }

    /**
     * @return string
     */
    public function getCode(){
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code){
        $this->code = $code;
    }



}