<?php

namespace app\models;

use Ramsey\Uuid\Uuid;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "emails".
 *
 * @property int $id
 * @property string $email
 * @property string|null $code
 * @property string|null $token
 * @property string|null $created_at
 */
class Email extends \yii\db\ActiveRecord
{

    public function init(){
        $this->on(self::EVENT_BEFORE_INSERT, function (){
            $this->code = $this->getRandomCode();
        });
    }


    private function getRandomCode(){
        $code = '';
        for ($i = 0; $i < 6; $i++) {
            $code .= rand(0, 9);
        }
        return $code;

    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails';
    }

    public function setToken(){
        $this->token = Uuid::uuid4()->toString();
        $this->save(false, ['token']);
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'code' => 'Code',
            'created_at' => 'Created At',
        ];
    }
}
