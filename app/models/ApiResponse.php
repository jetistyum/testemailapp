<?php


namespace app\models;


use yii\base\Model;

Abstract class ApiResponse extends Model implements \JsonSerializable
{
    public $errors;

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $this->getAttributes();
    }

}