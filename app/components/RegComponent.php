<?php


namespace app\components;


use app\models\Email;
use app\models\forms\EmailForm;
use yii\base\Component;
use yii\db\Expression;

class RegComponent extends Component
{
    /**
     * @param Email $email
     * @return bool
     */
    public function handleEmail(Email $email){
        /**
         * @var Email $email
         */
        $sent = \Yii::$app->getMailer()
            ->compose('code', ['code' => $email->code])
            ->setTo($email->email)
            ->setFrom('admin@localhost')
            ->send();
        if ($sent){
            $email->setToken();
            return true;
        }
        return false;
        
    }


    /**
     *
     * @param string $code
     * @return Email|null
     */
    public function findEmailByCode(string $code):?Email
    {
        return Email::find()->where(['code' => $code, 'token'=>null])
        ->andWhere(['>=', 'created_at', new Expression('now() - interval 5 minute')])->one();
    }

    /**
     *
     * @param string $token
     * @return Email|null
     */
    public function findEmailByToken(string $token):?Email
    {
        return Email::find()->where(['token' => $token])->one();
    }

    /**
     * @param EmailForm $emailForm
     * @return bool
     */
    public function createEmail(EmailForm $emailForm){

        $model = new Email();
        $model->email = $emailForm->email;

        if ($model->save()){
            $emailForm->setCode($model->code);
            return true;
        }
        return false;
    }
}