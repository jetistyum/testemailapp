<?php

use yii\db\Migration;

/**
 * Class m200303_222023_form
 */
class m200303_222023_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('forms', [
            'id'=>$this->primaryKey(),
            'title'=>$this->string(64),
        ]);

        $this->createTable('files', [
            'id'=>$this->primaryKey(),
            'filename'=>$this->string(64),
            'filepath'=>$this->string(256),
            'form_id'=>$this->integer()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('files' );
        $this->dropTable('forms' );
    }

}
