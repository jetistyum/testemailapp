<?php

use yii\db\Migration;

/**
 * Class m200227_224105_initial
 */
class m200227_224105_initial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('emails', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'code'=>$this->string(6),
            'token'=>$this->string(64),
            'created_at'=>$this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('emails');
    }

}
