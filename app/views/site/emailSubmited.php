<?php
/**
 *
 * @var string $code;
 *
 */
?>
<div class="jumbotron">
    This is your code:
    <p class="lead"><?=$code;?></p>
    <?=\yii\helpers\Html::a('Send email via API', ['api/send', 'code'=>$code]);?>
</div>
