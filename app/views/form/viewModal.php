<?php
/**
 * @var \app\models\Form $model
 *
 */

use yii\helpers\Html;

?>
<h2><?= Html::encode($model->title);?> </h2>
<?php
foreach ($model->files as $file):?>
    <div><?=Html::encode($file->filename);?></div>
<?php endforeach;?>