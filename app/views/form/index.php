<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/**
 * @var  \yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">Test 2</div>
    <div class="panel-body">
        <pre>
        Create an application that will allow to submit forms with one or more files and view the forms later.

        The page will have
        1)An "Add form" button that will open a "Add form" dialog for submiting new form.
        2)Table of submited forms that will show name and view button.

        Add form dialog will have :
        1) "Form name" - text input field
        2) An "Add file" - button
        3) A "Save Form" - button

        When "Add file" button will be clicked, it will show a new row with name(text field) and upload option.
        User can add and upload multiple files this way.
        Once form is saved, the dialog is closed.
            
        When we click on one of the saved forms, we will get a dialog that will alow us to view the submited files for this form. (No need to have edit option)

        Put the code on server of your choise and provide me a link to working env.
        Build it in a way you would make it , if it was a part of a system (in terms of comments, code style, classes...)
            </pre>
    </div>
</div>

<?php Modal::begin([
    'header' => '<h2>File Upload</h2>',
    'toggleButton' => ['label' => 'Add Form', 'class' => 'btn btn-default'],
]); ?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['/form/save']]) ?>
<?= $form->field($model, 'title')->textInput(); ?>

<div id="field-example" style="display: none">
    <?= $form->field($model, 'filename[]')->textInput(); ?>
    <?= $form->field($model, 'file[]')->fileInput(['multiple' => false]); ?>
</div>
<div id="field-container">

</div>
<?= Html::button('Add file', ['class' => 'btn btn-primary', 'onClick' => 'addField()']) ?> <?= Html::submitButton('Save Form', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end() ?>
<?php Modal::end(); ?>

<script>
    function addField() {
        if (typeof window.newfield == 'undefined') {
            window.newfield = $('#field-example>div.form-group').clone();
            $('#field-example>div.form-group').remove();
        }
        $('#field-container').append(window.newfield.clone());
    }

    $(function ($) {
        //JS script
        $('#formModal').on('hide.bs.modal', function(){
            $(this).find('.modal-body').html('');
        });

        $('.preview-modal').on('click', function (e) {
            e.preventDefault();
            $('#formModal').modal('show').find('.modal-body').load($(this).attr('href'));
        });
    });
</script>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        [
            'attribute' => 'title',
            'content' => function ($model) {
                return Html::a($model->title, ['form/view', 'id' => $model->id], ['class' => 'preview-modal']);
            }
        ]
        // ...
    ],
]) ?>

<?php
Modal::begin([
    'id' => 'formModal',
    'header' => 'Form view',
    'clientOptions' => [

    ]
]);
?>
<?php Modal::end(); ?>

