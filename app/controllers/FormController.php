<?php


namespace app\controllers;


use app\models\Form;
use app\models\forms\FileFormModel;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;

class FormController extends Controller
{
    public function actionIndex(){

        $query = Form::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
               ]
            ],
        ]);



        $model = new FileFormModel();
        return $this->render('index', ['model'=>$model, 'dataProvider'=>$dataProvider]);
    }

    public function actionSave(){
        $model = new FileFormModel();
        if ($model->load(\Yii::$app->request->post())){
            $model->file = UploadedFile::getInstances($model, 'file');
            $model->save();
            $this->redirect(['index']);
        }
    }


    public function actionView($id){
        $model = Form::findOne($id);
       return $this->renderAjax('viewModal', ['model'=>$model]);
    }
}